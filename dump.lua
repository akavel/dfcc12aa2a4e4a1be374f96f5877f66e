local function dump(list)
	local str = "{"
	local seen = {}
	for _,v in ipairs(list) do
		if type(v) ~= "table" then
			str = str .. tostring(v) .. ", "
		else
			str = str .. dump(v) .. ", "
		end
		seen[_] = true
	end
	if #list > 0 then
		str = str:sub(1, #str-2)
	end
	local trim = false
	for k,v in pairs(list) do
		if not seen[k] then
			trim = true
			if type(v) ~= "table" then
				str = str .. tostring(k) .. " = " .. tostring(v) .. ", "
			else
				str = str .. tostring(k) .. " = " .. dump(v) .. ", "
			end
		end
	end
	if trim then str = str:sub(1, #str-2) end
	str = str .. "}"
	return str
end